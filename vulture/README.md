#Vulture
"Vulture finds unused code in Python programs."

## Links
* https://github.com/jendrikseipp/vulture
* https://pypi.org/project/vulture/

## Install
`pip install vulture`

## Usage
* Help file: `vulture --help`
* Single file: `vulture ~/dev/tool_of_the_month/vulture/hello.py`
* Directory: `vulture ~/dev/tool_of_the_month/`
* Exclude files/folders: `vulture --exclude venv ~/dev/tool_of_the_month/`
