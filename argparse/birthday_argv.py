#!/usr/bin/env python3

import sys
from datetime import datetime
from dateutil import parser

# ./birthday_argv.py keith richards 12/18/1943
# ./birthday_argv.py 'keith richards' 12/18/1943
# ./birthday_argv.py 'keith richards' 'Dec 18, 1943'

name = sys.argv[1]
birthday_str = sys.argv[2]


birthday = parser.parse(birthday_str)
now = datetime.now()
age = now - birthday

print("{name} is {days:,} days old!".format(name=name, days=age.days))

