#!/usr/bin/env python3

import sys
from datetime import datetime
from dateutil import parser

# Wrong:
# ./birthday_argv.py keith richards 12/18/1943
# ./birthday_argv.py 12/18/1943

# Correct:
# ./birthday_argv.py 'keith richards' 12/18/1943
# ./birthday_argv.py 'keith richards' 'Dec 18, 1943'

def usage():
    print("{} 'name' 'date'".format(sys.argv[0]))


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Wrong number of parameters")
        usage()
        sys.exit(1)

    name = sys.argv[1]
    birthday_str = sys.argv[2]


    birthday = parser.parse(birthday_str)
    now = datetime.now()
    age = now - birthday

    print("{name} is {days:,} days old!".format(name=name, days=age.days))

