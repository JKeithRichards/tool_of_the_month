#!/usr/bin/env python3

import argparse
from datetime import datetime
from dateutil import parser as date_parser

parser = argparse.ArgumentParser()
parser.add_argument("name", help="Your name")
parser.add_argument("birthday", help="Your birthday")
args = parser.parse_args()

birthday = date_parser.parse(args.birthday)
now = datetime.now()
age = now - birthday

print("{name} is {days:,} days old!".format(name=args.name, days=age.days))

