#!/usr/bin/env python3

from datetime import datetime
from dateutil import parser

name = "Keith Richards"
birthday_str = "12/18/1943"

birthday = parser.parse(birthday_str)
now = datetime.now()
age = now - birthday

print("{name} is {days:,} days old!".format(name=name, days=age.days))

