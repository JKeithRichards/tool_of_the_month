#!/usr/bin/env python3

import argparse
from datetime import datetime

from dateutil import parser as date_parser
import emoji


parser = argparse.ArgumentParser()
parser.add_argument("-n", "--name", help="Your name", default="Anon")
parser.add_argument("-b", "--birthday", help="Your birthday", required=True)
parser.add_argument("-f", "--flare", action="store_true")
args = parser.parse_args()

birthday = date_parser.parse(args.birthday)
now = datetime.now()
age = now - birthday

if args.flare:
    if age.days <= 10950:
        print(emoji.emojize("{name} is {days:,} days old! :thumbs_up:".format(name=args.name, days=age.days)))
    else:
        print(emoji.emojize("{name} is {days:,} days old! :dizzy_face:".format(name=args.name, days=age.days)))
else:
    print("{name} is {days:,} days old!".format(name=args.name, days=age.days))

